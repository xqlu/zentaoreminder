运行前先修改配置文件，位置在安装目录下config.json

![image-20230921142104819](./image-20230921142104819.png)

具体配置项解释
基于 禅道开源版本18.6
```json
{
    "tokenUrl": "/api.php/v1/tokens",
    "password": "123asdasd@2030LZ",
    "myBlockUrl": "/block-printBlock-1-my.json",
    "zenTaoHost": "http://127.0.0.1/zentao",
    "account": "admin"
}
```

1.tokennUrl 这是获取token 的一个 具体可见 [获取Token - 禅道二次开发手册 - 禅道开源项目管理软件 (zentao.net)](https://www.zentao.net/book/api/664.html)

2.zenTaoHost 禅道的主地址 如果没有nginx 转发 ，一般访问 http://ip:port/zentao 即可。若使用了nginx ，得自己尝试找找。

3.myBlockUrl 获取bugs数量的接口 这个不存在于接口文档，得自己去找。请求后缀html->json 会获取到json格式的数据

![image-20230921143756147](./image-20230921143756147.png)

功能列表:
1.绿灯常亮 禅道账户登录正常 也没有BUG
2.红灯闪烁 禅道账户登录正常 但有BUG
3.黄灯闪烁 禅道账户登录不正常

运行效果图
![image-20230921141427642](./image-20230921141427642.png)

[exe文件](./zentao_reminder-1.0.exe)
