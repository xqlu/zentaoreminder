package com.xqlu;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.StaticLog;

import java.util.Objects;

/**
 * @author luxiaoqiang
 * @version 1.0.0
 * @description 禅道功能汇聚类
 * @title ZenTaoClient
 * @create 2023/9/19 15:40
 * @since 1.0.0
 */
public class ZenTaoClient {

    /**
     * 一般的禅道访问地址 应该是类似 http://127.0.0.1/zentao/api.php/v1/tokens 这种。但是从浏览器请求响应来看 公司禅道用了ngix转发 所以地址中没得zentao
     */
    private String host;
    private String account;
    private String password;
    private String tokenUrl;
    private String myBlockUrl;

    public ZenTaoClient(Config config) {
        this.host = config.getZenTaoHost();
        this.account = config.getAccount();
        this.password = config.getPassword();
        this.tokenUrl = config.getTokenUrl();
        this.myBlockUrl = config.getMyBlockUrl();
    }

    private final String TOKEN_FLAG = "token";

    public String requestToken() {
        Object tokenObj = LocalCacheManager.get(TOKEN_FLAG);
        if (Objects.isNull(tokenObj)) {
            return requestToken0();
        }
        return tokenObj.toString();
    }

    private String requestToken0() {
        JSONObject paramMap = new JSONObject();
        paramMap.set("account", account);
        paramMap.set("password", password);
        StaticLog.info("requestToken url{}", host + tokenUrl);
        String post = HttpUtil.post(host + tokenUrl, paramMap.toString(), 1000 * 60);
        StaticLog.info("requestToken resp{}", post);
        if (Objects.nonNull(post) && post.contains(TOKEN_FLAG)) {
            JSONObject resp = JSONUtil.parseObj(post);
            String token = resp.getStr(TOKEN_FLAG);
            //缓存20分钟
            LocalCacheManager.put(TOKEN_FLAG, token, 20 * 60 * 1000);
            return token;
        }
        return null;
    }

    /**
     * 请求block模块的bugs值
     *
     * @return
     */
    public Integer myBlockBugs() {
        try {
            String token = requestToken();
            if (StrUtil.isBlank(token)) {
                return null;
            }
            HttpRequest request = HttpUtil.createGet(host + myBlockUrl);
            StaticLog.info("request url{}", request.getUrl());
            request.header(TOKEN_FLAG, token);
            request.setConnectionTimeout(60 * 1000);
            HttpResponse response = request.execute();
            String respStr = response.body();
            if (response.isOk()) {
                JSONObject body = JSONUtil.parseObj(respStr);
                return body.getJSONObject("data").getInt("bugs");
            }
            StaticLog.info("myBlockBugs resp{}", respStr);
        } catch (Exception e) {
            e.printStackTrace();
            StaticLog.error(e);
            return null;
        }
        return null;
    }

    public static void main(String[] args) {

    }
}
