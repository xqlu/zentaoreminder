package com.xqlu;

/**
 * @author luxiaoqiang
 * @version 1.0.0
 * @description 配置
 * @title LogManager
 * @create 2023/9/20 13:48
 * @since 1.0.0
 */
public class Config {
    /**
     * 禅道服务器地址
     */
    private String zenTaoHost;
    /**
     * 登录用户
     */
    private String account;
    /**
     * 登录密码
     */
    private String password;

    /**
     * token url
     */
    private String tokenUrl;

    /**
     * 我的bugs url
     */
    private String myBlockUrl;

    public String getZenTaoHost() {
        return zenTaoHost;
    }

    public void setZenTaoHost(String zenTaoHost) {
        this.zenTaoHost = zenTaoHost;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getTokenUrl() {
        return tokenUrl;
    }

    public void setTokenUrl(String tokenUrl) {
        this.tokenUrl = tokenUrl;
    }

    public String getMyBlockUrl() {
        return myBlockUrl;
    }

    public void setMyBlockUrl(String myBlockUrl) {
        this.myBlockUrl = myBlockUrl;
    }
}
