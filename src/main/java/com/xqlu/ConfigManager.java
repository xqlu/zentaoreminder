package com.xqlu;

import cn.hutool.core.io.FileUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.log.StaticLog;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * @author luxiaoqiang
 * @version 1.0.0
 * @description 配置管理器
 * @title LogManager
 * @create 2023/9/20 13:48
 * @since 1.0.0
 */
public class ConfigManager {
    public static String ROOT_DIR_PATH = System.getProperty("user.dir");

    public static Config load() {
        File configFile = new File(ROOT_DIR_PATH + File.separator + "config.json");
        if (configFile.exists() && configFile.length() > 0) {
            String configJson = FileUtil.readString(configFile, Charset.forName("UTF-8"));
            JSONObject jsonObject = JSONUtil.parseObj(configJson);
            if (jsonObject.isEmpty()) {
                return null;
            }
            return JSONUtil.parseObj(configJson).toBean(Config.class);
        } else {
            try {
                configFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                StaticLog.error(e);
            }
            Config initConfig = new Config();
            initConfig.setZenTaoHost("禅道地址");
            initConfig.setAccount("账户");
            initConfig.setPassword("密码");
            initConfig.setTokenUrl("禅道请求token地址,默认:/api.php/v1/tokens");
            initConfig.setMyBlockUrl("禅道请求bugs地址,默认:/index.php?m=block&f=printBlock&id=311&module=my&t=json");
            FileUtil.writeString(JSONUtil.toJsonStr(initConfig), configFile, Charset.forName("UTF-8"));
        }
        return null;
    }

    public static void main(String[] args) {
        Config config = JSONUtil.parseObj("{}").toBean(Config.class);
        System.out.println(config.getAccount());
    }

}
