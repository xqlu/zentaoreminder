//package com.xqlu;
//
//import cn.hutool.core.io.FileUtil;
//import cn.hutool.json.JSONObject;
//import cn.hutool.json.JSONUtil;
//import cn.hutool.log.StaticLog;
//
//import java.io.File;
//import java.nio.charset.Charset;
//import java.util.concurrent.ConcurrentHashMap;
//
//import static com.xqlu.ConfigManager.ROOT_DIR_PATH;
//
///**
// * @author luxiaoqiang
// * @version 1.0.0
// * @description 数据持久化器 持久化到应用指定文件夹下
// * @title LocalPersistManager
// * @create 2023/9/19 15:40
// * @since 1.0.0
// */
//public class LocalPersistManager {
//    private ConcurrentHashMap<String, Object> DATA_MAP = new ConcurrentHashMap(10);
//    private static File dataFile;
//    private static final LocalPersistManager INSTANCE = new LocalPersistManager();
//
//    static {
//        try {
//            dataFile = new File(ROOT_DIR_PATH + File.separator + "record.json");
//            if (!dataFile.exists()) {
//                dataFile.createNewFile();
//            } else {
//                if (dataFile.length() > 0) {
//                    String recordStr = FileUtil.readString(dataFile, Charset.forName("UTF-8"));
//                    JSONObject jsonObject = JSONUtil.parseObj(recordStr);
//                    if (!jsonObject.isEmpty()) {
//                        INSTANCE.DATA_MAP = jsonObject.toBean(ConcurrentHashMap.class);
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            StaticLog.error(e);
//        }
//    }
//
//
//    private LocalPersistManager() {
//    }
//
//    public static boolean save(String key, Object value) {
//        if (key == null || value == null) {
//            return false;
//        }
//        INSTANCE.DATA_MAP.put(key, value);
//        FileUtil.writeString(JSONUtil.toJsonStr(INSTANCE.DATA_MAP), dataFile, Charset.forName("UTF-8"));
//        return true;
//    }
//
//    public static Object read(String key) {
//        if (key == null) {
//            return null;
//        }
//        return INSTANCE.DATA_MAP.get(key);
//    }
//
//    public static void main(String[] args) {
//        for (int i = 0; i < 10; i++) {
//            LocalPersistManager.save("TOKEN" + i, "TOKEN" + i);
//            System.out.println(String.valueOf(LocalPersistManager.read("TOKEN" + i)));
//        }
//    }
//}
