package com.xqlu;

import javax.swing.*;
import java.awt.*;

/**
 * @author luxiaoqiang
 * @version 1.0.0
 * @description 资源管理类
 * @title ResourceManager
 * @create 2023/9/20 13:48
 * @since 1.0.0
 */
public class ResourceManager {

    public static Image GREEN_LIGHT;
    public static Image YELLOW_LIGHT;
    public static Image RED_LIGHT;
    public static Image DEFAULT_LIGHT;

    static {
        GREEN_LIGHT = Toolkit.getDefaultToolkit().getImage(ResourceManager.class.getResource("/img/green.png"));
        YELLOW_LIGHT = Toolkit.getDefaultToolkit().getImage(ResourceManager.class.getResource("/img/yellow.png"));
        RED_LIGHT = Toolkit.getDefaultToolkit().getImage(ResourceManager.class.getResource("/img/red.png"));
        DEFAULT_LIGHT = new ImageIcon("").getImage();
    }


}
