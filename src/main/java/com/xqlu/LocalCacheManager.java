package com.xqlu;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author luxiaoqiang
 * @version 1.0.0
 * @description 本地缓存管理器
 * @title LocalCacheManager
 * @create 2023/9/20 14:02
 * @since 1.0.0
 */
public class LocalCacheManager {
    final private ConcurrentHashMap<String, CacheItem> CACHE_MAP = new ConcurrentHashMap(10);
    private static final LocalCacheManager INSTANCE = new LocalCacheManager();

    private LocalCacheManager() {
    }

    public static Object get(String key) {
        if (key == null) {
            return null;
        }
        CacheItem cacheItem = INSTANCE.CACHE_MAP.get(key);
        if (Objects.isNull(cacheItem)) {
            return null;
        }
        if (cacheItem.expireTime > System.currentTimeMillis()) {
            return cacheItem.value;
        } else {
            INSTANCE.CACHE_MAP.remove(key);
        }
        return null;
    }

//    public static void put(String key, Object value) {
//        put(key, value);
//    }

    public static void put(String key, Object value, long timeout) {
        if (key == null || value == null) {
            return;
        }
        INSTANCE.CACHE_MAP.put(key, new CacheItem(timeout, value));
    }

    private static class CacheItem {
        /**
         * 缓存放入时间，毫秒
         */
        final private long expireTime;

        final private Object value;

        public CacheItem(long expireTime, Object value) {
            this.expireTime = expireTime;
            this.value = value;
        }
    }

}