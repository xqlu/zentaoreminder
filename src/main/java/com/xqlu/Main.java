package com.xqlu;

import cn.hutool.log.StaticLog;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.awt.*;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static com.xqlu.ConfigManager.ROOT_DIR_PATH;

/**
 * 实现消息提醒效果
 *
 * @author zjw
 * @createTime 2020/9/13 15:37
 */
public class Main extends Application {

    private TrayIcon trayIcon;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        // 隐藏窗口及任务栏图标、只显示托盘图标
        stage.initStyle(StageStyle.UTILITY);
        stage.setOpacity(0);

        // 添加托盘图标
        setTrayIcon();

        // 开始闪烁逻辑
        startTwinkleBiz();

        stage.show();
        StaticLog.info("应用根目录： {}", ROOT_DIR_PATH);

    }

    private void startTwinkleBiz() {
        Config config = ConfigManager.load();
        if (Objects.isNull(config)) {
            StaticLog.info("配置为空");
            trayIcon.setImage(ResourceManager.YELLOW_LIGHT);
            return;
        }
        ZenTaoClient zenTaoClient = new ZenTaoClient(config);
        setTimerTask(zenTaoClient);
    }

    private void setTimerTask(ZenTaoClient zenTaoClient) {
        AtomicReference<Integer> myBlockBugs = new AtomicReference<>(zenTaoClient.myBlockBugs());
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);
        executorService.scheduleAtFixedRate(() -> {
            myBlockBugs.set(zenTaoClient.myBlockBugs());
        }, 0, 30, TimeUnit.SECONDS);


        Thread bugListenerThread = new Thread(() -> {
            while (true) {
                if (Objects.isNull(myBlockBugs.get())) {
                    twinkleLight(ResourceManager.YELLOW_LIGHT);
                } else if (Objects.equals(myBlockBugs.get(), 0) && !trayIcon.getImage().equals(ResourceManager.GREEN_LIGHT)) {
                    trayIcon.setImage(ResourceManager.GREEN_LIGHT);
                } else if (myBlockBugs.get() > 0) {
                    twinkleLight(ResourceManager.RED_LIGHT);
                }
            }
        });
        bugListenerThread.start();
    }

    private void twinkleLight(Image light) {
        trayIcon.setImage(light);
        sleep(290);
        trayIcon.setImage(ResourceManager.DEFAULT_LIGHT);
        sleep(290);
    }

    private void setTrayIcon() {
        SystemTray systemTray = SystemTray.getSystemTray();

        // 添加退出菜单项、并设置监听事件
        PopupMenu menu = new PopupMenu();
        MenuItem quit = new MenuItem("退出");
        menu.add(quit);
        quit.addActionListener(e -> System.exit(0));

        // 设置托盘图标
        trayIcon = new TrayIcon(ResourceManager.DEFAULT_LIGHT, "禅道提醒器", menu);

        // 让图片自适应、防止图标尺寸不对导致无法显示
        trayIcon.setImageAutoSize(true);

        try {
            systemTray.add(trayIcon);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }


    private void sleep(int millions) {
        try {
            Thread.sleep(millions);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
